/**
Copyright (c) 2020,2021 PickData SL (https://www.pickdata.net/)
All rights reserved.
emod-js-simulator - The BSD 3-Clause License
**/

const ai7pr2 = require("./src/7AI2PR");
const sr8 = require("./src/8SR");
const di10 = require("./src/10DI");

module.exports.AI7PR2 = ai7pr2;
module.exports.SR8 = sr8;
module.exports.DI10 = di10;
