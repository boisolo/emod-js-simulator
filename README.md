# emod-js-simulator

Simulator of PickData eMod modules. This package is only a dependency of others emod packages, like [node-red-contrib-emod-simulator](https://www.npmjs.com/package/@pickdata/node-red-contrib-emod-simulator).
