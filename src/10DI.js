/**
Copyright (c) 2020,2021 PickData SL (https://www.pickdata.net/)
All rights reserved.
emod-js-simulator - The BSD 3-Clause License
**/

class DI10 {
    constructor() {
        this.inputs_status = new Array(10).fill(false);
        this.inputs_pulse_counter = new Array(10).fill(0);
        this.inputs_pulse_filter_time = new Array(10).fill(0);
        this.inputs_pulse_counter_threshold = 1;
        this.inputs_timers = new Array(10).fill(null);
        this.inputs_timer_min = new Array(10).fill(500);
        this.inputs_timer_max = new Array(10).fill(1500);
        this.inputs_last_change = new Array(10).fill(-1);
        this.inputs_last_counter = new Array(10).fill(0);
        this.input_change_callback = null;
        this.input_pulse_counter_callback = null;
    }

    stopSimulation() {
        for (var i = 0; i < 10; i++) {
            if (this.inputs_timers[i] != null) {
                clearTimeout(this.inputs_timers[i]);
                this.inputs_timers[i] = null;
            }
        }
    }

    startSimulation() {
        this.stopSimulation();
        for (var i = 0; i < 10; i++) {
            this.inputs_timers[i] = setTimeout(this._inputChange.bind(this), this._getInputChangeTimeout(i), i, true);
        }
    }

    _getInputChangeTimeout(index) {
        if (!Number.isInteger(index) || index < 0 || index > 9) {
            throw new Error('index must be an integer between 0 and 9');
        }

        return Math.floor(Math.random() * (this.inputs_timer_max[index] - this.inputs_timer_min[index] + 1) + this.inputs_timer_min[index]);
    }

    _inputChange(index, new_value) {
        var now = Date.now();

        this.inputs_status[index] = new_value;
        if (this.input_change_callback != null) {
            const inputs_status = [...this.inputs_status];
            this.input_change_callback(inputs_status);
        }

        if (now - this.inputs_last_change[index] >= this.inputs_pulse_filter_time[index]) {
            this.inputs_pulse_counter[index]++;

            if ((this.inputs_pulse_counter[index] - this.inputs_last_counter[index]) >= this.inputs_pulse_counter_threshold) {
                if (this.input_pulse_counter_callback != null) {
                    this.input_pulse_counter_callback(index, this.inputs_pulse_counter[index]);
                }

                this.inputs_last_counter[index] = this.inputs_pulse_counter[index];
            }
        }
        
        this.inputs_timers[index] = setTimeout(this._inputChange.bind(this), this._getInputChangeTimeout(index), index, !new_value);
        this.inputs_last_change[index] = now;
    }

    setThreshold(threshold) {
        this.inputs_pulse_counter_threshold = threshold;
    }

    setStatusChangeCallback(cb) {
        this.input_change_callback = cb;
        if (cb != null) {
            const inputs_status = [...this.inputs_status];
            this.input_change_callback(inputs_status);
        }
    }

    setPulseCounterCallback(cb) {
        this.input_pulse_counter_callback = cb;
        if (cb != null) {
            for (var i = 0; i < 10; i++) {
                this.input_pulse_counter_callback(i, this.inputs_pulse_counter[i]);
            }
        }
    }

    setPulseFilterTime(inputs_pulse_filter_time_array) {
        if (!Array.isArray(inputs_pulse_filter_time_array) || inputs_pulse_filter_time_array.length != 10) {
            throw new Error('inputs_pulse_filter_time_array must be an array of 10 elements');
        }

        for (var i = 0; i < 10; i++) {
            if (Number.isInteger(inputs_pulse_filter_time_array[i]) && inputs_pulse_filter_time_array[i] < 0) {
                this.inputs_pulse_filter_time[i] = inputs_pulse_filter_time_array[i];
            } else {
                // emod-controller-js-bindings equivalent method does implicit conversion to 0 if value is not a valid integer.
                this.inputs_pulse_filter_time[i] = 0;
            }

            if (inputs_pulse_filter_time_array[i] != 0) {
                this.inputs_timer_min[i] = inputs_pulse_filter_time_array[i] / 2;
                this.inputs_timer_max[i] = inputs_pulse_filter_time_array[i] * 2;
            } else {
                this.inputs_timer_min[i] = 500;
                this.inputs_timer_max[i] = 1500;
            }
        }
    }

    resetAllPulseCounter() {
        for (var i = 0; i < 10; i++) {
            this.resetPulseCounter(i);
        }
    }

    resetPulseCounter(index) {
        if (!Number.isInteger(index) || index < 0 || index > 9) {
            throw new Error('index must be an integer between 0 and 9');
        }

        this.inputs_pulse_counter[index] = 0;
    }

    getAllStatus() {
        const inputs_status = [...this.inputs_status];

        return inputs_status;
    }

    getOneStatus(index) {
        if (!Number.isInteger(index) || index < 0 || index > 9) {
            throw new Error('index must be an integer between 0 and 9');
        }

        const inputs_status = this.getAllStatus();

        return inputs_status[index];
    }

    getAllCounters() {
        const inputs_pulse_counter = [...this.inputs_pulse_counter];

        return inputs_pulse_counter;
    }

    getOneCounter(index) {
        if (!Number.isInteger(index) || index < 0 || index > 9) {
            throw new Error('index must be an integer between 0 and 9');
        }

        const inputs_pulse_counter = this.getAllCounters();

        return inputs_pulse_counter[index];
    }
}

class SingletonDI10 {
    constructor() {
        throw new Error('Use getInstance(num)');
    }
    
    static getInstance(num) {
        if (!Number.isInteger(num) || num < 1 || num > 5) {
            throw new Error('Module number must be an integer between 1 and 5');
        }

        if (!SingletonDI10.instance) {
            SingletonDI10.instance = new Array(5).fill(null);;
        }

        if (!SingletonDI10.instance[num-1]) {
            SingletonDI10.instance[num-1] = new DI10();
        }

        return SingletonDI10.instance[num-1];
    }
}

module.exports = SingletonDI10;
